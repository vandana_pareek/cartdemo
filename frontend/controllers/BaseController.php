<?php

namespace frontend\controllers;


use common\models\CartItem;

class BaseController extends \yii\web\Controller
{
    public function beforeAction($action)
    {

        $this->view->params['cartItemCount'] = CartItem::getTotalQuantityForUser(\Yii::$app->user->id);
        return parent::beforeAction($action);
    }
}