<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1>Customer Details</h1>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'firstname',
            'lastname',
            'username',
            'email:email',
            [
                'label' => 'status',
                'value' =>  $model->status === 9 ? 'Inactive' : 'Active'
            ],
            'created_at:date',
            'updated_at:date',
        ],
    ]) ?>

</div>
<style>
    .content-header h1{
        display: none;
    }
    </style>