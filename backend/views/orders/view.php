<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Orders */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="orders-view">

    <h1>Order Details</h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'total_price',
            'status',
            'firstname',
            'lastname',
            'email:email',
            [
                'label' => 'Address',
                'value' => $model->orderAddresses->address
            ],
            [
                'label' => 'City',
                'value' => $model->orderAddresses->city
            ],
            [
                'label' => 'State',
                'value' => $model->orderAddresses->state
            ],
            [
                'label' => 'Country',
                'value' => $model->orderAddresses->country
            ],
            [
                'label' => 'Zipcode',
                'value' => $model->orderAddresses->zipcode
            ],
            'transaction_id',
            'created_at:date',
        ],
    ]) ?>

</div>
<style>
    .content-header h1{
        display: none;
    }
    </style>