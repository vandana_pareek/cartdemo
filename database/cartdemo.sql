-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 28, 2021 at 02:28 PM
-- Server version: 5.7.33-0ubuntu0.18.04.1
-- PHP Version: 7.3.12-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cartdemo`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart_items`
--

CREATE TABLE `cart_items` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(2) NOT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1614324015),
('m130524_201442_init', 1614324027),
('m190124_110200_add_verification_token_column_to_user_table', 1614324027),
('m201124_064118_create_products_table', 1614330781),
('m201124_064518_create_user_addresses_table', 1614330781),
('m201124_064907_create_orders_table', 1614330781),
('m201124_065209_create_order_items_table', 1614330781),
('m201124_065518_create_order_addresses_table', 1614330781),
('m201124_066009_create_cart_items_table', 1614330781),
('m201212_081304_add_firstname_lastname_columns_to_user_table', 1614330781),
('m201217_061032_add_paypal_order_id_column_to_orders_table', 1614330781),
('m201226_063616_add_admin_column_to_user_table', 1614330781),
('m201226_071858_change_product_id_foreign_key_on_order_item_table', 1614330781);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `email` varchar(255) NOT NULL,
  `transaction_id` varchar(255) DEFAULT NULL,
  `paypal_order_id` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `total_price`, `status`, `firstname`, `lastname`, `email`, `transaction_id`, `paypal_order_id`, `created_at`, `created_by`) VALUES
(31, '209.00', 0, 'vandana', 'pareek', 'vandana@gmail.com', NULL, NULL, 1614499119, 7),
(32, '143.00', 0, 'test user', '.', 'test user 56', NULL, NULL, 1614499307, NULL),
(33, '22.00', 0, 'test', 'test', 'test@gmsil.com', NULL, NULL, 1614499804, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order_addresses`
--

CREATE TABLE `order_addresses` (
  `order_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `zipcode` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_addresses`
--

INSERT INTO `order_addresses` (`order_id`, `address`, `city`, `state`, `country`, `zipcode`) VALUES
(31, 'test address', 'test city', 'test', 'test', '123456'),
(32, 'test test test', 'test', 'test', 'test', '12345'),
(33, 'test', 'test', 'test', 'test', '4324234');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `order_id` int(11) NOT NULL,
  `quantity` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `product_name`, `product_id`, `unit_price`, `order_id`, `quantity`) VALUES
(34, 'Test Product 1', 1, '22.00', 31, 1),
(35, 'Test Product 2', 2, '44.00', 31, 3),
(36, 'Test Product 3', 3, '55.00', 31, 1),
(37, 'Test Product 2', 2, '44.00', 32, 2),
(38, 'Test Product 3', 3, '55.00', 32, 1),
(39, 'Test Product 1', 1, '22.00', 33, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` longtext,
  `image` varchar(2000) DEFAULT NULL,
  `price` decimal(10,2) NOT NULL,
  `status` tinyint(2) NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `image`, `price`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Test Product 1', 'test description123456', '/products/ZW5oMnfAGKSA2uK1kvvmnyecnpwA1jXx/Screenshot from 2020-01-15 18-04-59.png', '22.00', 1, 1614341323, 1614497332, NULL, 1),
(2, 'Test Product 2', 'test description', '/products/tO7Dg5vhfQyafkAXr3WSKjtLfdJ3wSSg/Screenshot from 2020-01-15 18-12-36.png', '44.00', 1, 1614341451, 1614341451, 1, 1),
(3, 'Test Product 3', 'test description', '/products/0cTnhtffsUWlSQlkAlIv5pJObQ-MKtVr/Screenshot from 2020-01-15 18-12-02.png', '55.00', 1, 1614341556, 1614341556, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `admin` tinyint(1) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `admin`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'admin', 'admin', 'admin', NULL, '$2y$13$mDvLVbNPp0P78REF6cOctuGHXedY4vMvP9iSryhdwJaGww1wuEeIu', NULL, 'admin@example.com', 10, 1, 1614330878, 1614330878, NULL),
(2, 'test1', 'lastname1', 'test', 'Nvc33EtKnbX0iAyROFmjDHY7zzEbh8T6', '$2y$13$w25TkcQTTMrx6aQW2syRjuC5OKWrKmLYRgYi1THSPSYV9yHND22Ki', NULL, 'test@gmail.com', 9, NULL, 1614496156, 1614496156, 'nmOEFXGsSfj17lPou3U6_PSz9sgeNGFa_1614496156'),
(3, 'test2', 'lastname2', 'test1', '99h1ZXXqSkG2X5sHCI4YgVQgTwH7e_rb', '$2y$13$JR47pL3GwCdNHhc/QaJCreIdz3WclQbn0iMoOlP8Yx9IPJSyxQb12', NULL, 'test1@gmail.com', 10, NULL, 1614496208, 1614496208, 'reMNhaLUyg_Cs7kWcFx8MAv7vW_VfakI_1614496208'),
(4, 'test3', 'lastname3', 'test1234', '9ncd6OGq-wwGAdfnVBMwWUeKEy_YdMoj', '$2y$13$Y0bcMPu1DMwtzRo7fJSqweflUvjzRq5B77fILTP/ylp5c3B/FIKPy', NULL, 'test123@gmail.com', 10, NULL, 1614496533, 1614496533, 'VgDLlUjtI62il17dhitjLkQPxjl3AK7u_1614496533'),
(5, 'aman', 'pareek', 'aman@gmail.com', 'opXLBCrIgmk3RnscHPpS1GqDRCUh9ybU', '$2y$13$DwxWiUGp5EukH/oHvdauv.Tr.PVOlTBhYKmth/SaBv8snvn.bswZC', NULL, 'aman@gmail.com', 10, NULL, 1614496623, 1614496623, 'RNZHGnSDt1qvM6ZuL6BZQc-YesYVUJ9z_1614496623'),
(7, 'vandana', 'pareek', 'vandana', '_O0v6OSJaI2Wntn1rUE916DI7onEsqRa', '$2y$13$/iOhCowaSFiMhH.zyErZ/.6uG6y97H2ccvagmmt2tizVdF96dNFTa', NULL, 'vandana@gmail.com', 10, NULL, 1614497618, 1614497618, 'Ny9EYSDBSb42c5bif-OZtn8zHIPYbLKG_1614497618'),
(8, 'test user', 'test user', 'test56', 'f4u5WhGAYFB4WhfeAJj5xBgOqTn6YCJG', '$2y$13$gqw9Z1DrEe7b4FfbaBboVuEDElew/qF.IwiwGwJkZFFLWrNiPd0bK', NULL, 'test56@gmail.com', 9, NULL, 1614499257, 1614499257, 'Rr4lolcHNTShAD2cbJyEWEXE4NDlJ4wr_1614499257');

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `zipcode` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart_items`
--
ALTER TABLE `cart_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-cart_items-product_id` (`product_id`),
  ADD KEY `idx-cart_items-created_by` (`created_by`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-orders-created_by` (`created_by`);

--
-- Indexes for table `order_addresses`
--
ALTER TABLE `order_addresses`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `idx-order_addresses-order_id` (`order_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-order_items-product_id` (`product_id`),
  ADD KEY `idx-order_items-order_id` (`order_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-products-created_by` (`created_by`),
  ADD KEY `idx-products-updated_by` (`updated_by`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-user_addresses-user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart_items`
--
ALTER TABLE `cart_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cart_items`
--
ALTER TABLE `cart_items`
  ADD CONSTRAINT `fk-cart_items-created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-cart_items-product_id` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk-orders-created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_addresses`
--
ALTER TABLE `order_addresses`
  ADD CONSTRAINT `fk-order_addresses-order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `fk-order_items-order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk-products-created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-products-updated_by` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD CONSTRAINT `fk-user_addresses-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
